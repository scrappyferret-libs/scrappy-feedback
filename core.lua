--- Required libraries.
local json = require( "json" )
local lfs = require( "lfs" )

-- Localised functions.
local encode = json.encode
local date = os.date
local time = os.time
local pathForFile = system.pathForFile
local remove = os.remove
local open = io.open
local close = io.close
local len = string.len
local insert = table.insert
local find = string.find
local sub = string.sub

-- Localised values

-- Static values.

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

	-- Set the api url if required
	self:setAPIUrl( self._params.apiUrl )

	-- Set the onFeedback callback, if required
	self:setCallback( self._params.onFeedback )

	self._activationKeys = self._params.activationKeys or { "f12" }

	self._buttonCount = self._params.buttonCount or 5

	self._visual = display.newGroup()

	local back = display.newRect( self._visual, 0, 0, display.contentWidth * 0.35, display.contentHeight * 0.9 )
	back:setFillColor( 0, 0, 0, 0.95 )

	back:setStrokeColor( 1, 1, 1, 0.5 )
	back.strokeWidth = 1

	local options =
	{
	    text = "Feedback",
	    x = 0,
	    y = 0,
	    font = native.systemFont,
	    fontSize = 18,
	}

	local title = display.newText( options )
	self._visual:insert( title )
	title:setFillColor( 1, 1, 1 )
	title.x = 0
	title.y = - self._visual.contentHeight * 0.5 + title.contentHeight * 0.5

	local textBox = display.newRect( self._visual, 0, 0,  back.contentWidth * 0.9, back.contentHeight * 0.4 )
	textBox:setFillColor( 1, 1, 1, 0.25 )

	textBox:setStrokeColor( 1, 1, 1, 1 )
	textBox.strokeWidth = 1
	textBox.y = title.y + title.contentHeight + textBox.contentHeight * 0.5

	self._visual.x = ( self._visual.contentWidth * 0.5 ) + ( self._visual.contentWidth * 0.1 )
	self._visual.y = display.contentCenterY

	self._textBox = native.newTextBox( self._visual.x, 10000, back.contentWidth * 0.9, back.contentHeight * 0.4 )
	self._textBox.isEditable = true
	self._textBox.isFontSizeScaled = true
	self._textBox.size = 12

	local buttons = display.newGroup()

	for i = 1, self._buttonCount, 1 do

		local onTouch = function( event )
			if event.phase == "began" then
				event.target.isFocus = true
				display.getCurrentStage():setFocus( event.target, event.id )
			elseif event.target.isFocus then
				if event.phase ~= "moved" then
					event.target.isFocu = false
					display.getCurrentStage():setFocus( nil, event.id )
					self:submit( i )
				end
			end

			return true
		end

		local button = display.newCircle( buttons, 0, 0, ( back.contentWidth * ( 1 / self._buttonCount ) * 0.5 ) * 0.9 )
		button.x = ( button.contentWidth * ( i - 1 ) ) + ( button.contentWidth * 0.5 ) + ( ( button.contentWidth * 0.1 ) * ( i - 1 ) )
		button:addEventListener( "touch", onTouch )

	end

	buttons.x = back.x - buttons.contentWidth * 0.5
	buttons.y =  self._visual.contentHeight * 0.5 - buttons.contentHeight * 0.6
	self._visual:insert( buttons )
	--self._textBox.y = title.y --+ title.contentHeight * 0.5 --+ self._textBox.contentHeight * 0.5

	self._screenshot = display.newRect( self._visual, 0, 0, display.contentWidth * 0.1, display.contentHeight * 0.1 )
	self._screenshot.x = textBox.x + textBox.contentWidth * 0.5 - self._screenshot.contentWidth * 0.5
	self._screenshot.y = buttons.y - buttons.contentHeight * 0.5 - self._screenshot.contentHeight


    -- Register the handler for unhandled errors
    Runtime:addEventListener( "key", self )

	-- Register the user input listener
	self._textBox:addEventListener( "userInput", self )

	-- Close us straight away
	self:close( true )

	-- Error file extension
	self._extension = ".feedback"

	-- Calculate the length of a crash file name
	self._filenameLength = len( time() .. self._extension )

	-- Submit any locally stored feedback files that haven't been submitted yet
	if system.getInfo( "platform" ) ~= "nx64" then
		self:_submitFeedbackFiles()
	end
	
end


-- Sets the url for the external API that errors are submitted to.
-- @param url The url.
function library:setAPIUrl( url )
	self._apiURL = url
end

--- Gets the api url, if set.
-- @return The url.
function library:getAPIUrl()
	return self._apiURL
end

-- Sets the onFeedback callback.
-- @param callback The callback.
function library:setCallback( callback )
	self._callback = callback
end

--- Gets the onFeedback, if set.
-- @return The callback.
function library:getCallback()
	return self._callback
end

--- Key event handler.
-- @param event The event table.
function library:key( event )

	if event.phase == "down" then
		for i = 1, #self._activationKeys, 1 do
			if event.keyName == self._activationKeys[ i ] then
				if self:isOpen() then
					self:close()
					return true
				else
					self:open()
				end
			end
		end
	end

end

--- Key event handler.
-- @param event The event table.
function library:userInput( event )
	if self:key{ phase = "down", keyName = event.newCharacters } then
		self._textBox.text = string.gsub( self._textBox.text, event.newCharacters, "" )
	end
end

--- Updates the library.
function library:update( )
	self._visual:toFront()
end

function library:open()

	if not self:isDisabled() then

		local filename = "scrappyFeedback-" .. time() .. ".jpg"

		display.save( display.getCurrentStage(), filename )

		self._isOpen = true
		self._visual.isVisible = true
		self._textBox.isVisible = true
		native.setKeyboardFocus( self._textBox )


		self._screenshot.fill =
		{
		    type = "image",
		    filename = filename,
		    baseDir = system.DocumentsDirectory
		}

		self._screenshot.filename = filename

	end

end

function library:submit( buttonIndex )

	-- Create the feedback table from the event
	local feedback = self:_createFeedback( buttonIndex )

	-- Create a feedback file and get the filename and path
	local filename, path = self:_createFeedbackFile( feedback )

	-- Submit the feedback to an online api
	self:_submitFeedback( filename, path )

	-- Delete the screenshot file
	remove( pathForFile( self._screenshot.filename, system.DocumentsDirectory ) )

	self:close()

end


--- Creates a feedback table.
-- @param event The buttonIndex index that was pressed for the submission.
-- @return The feedback table.
function library:_createFeedback( buttonIndex )

	-- Create the default feedback
	local feedback =
	{
        date = date( "%c" ),
		buttonIndex = buttonIndex,
		text = self._textBox.text,
        appName = system.getInfo( "appName" ),
        appVersionString = system.getInfo( "appVersionString" ),
        architectureInfo = system.getInfo( "architectureInfo" ),
        solarBuild = system.getInfo( "build" ),
        textureMemoryUsed = system.getInfo( "textureMemoryUsed" ),
        manufacturer = system.getInfo( "manufacturer" ),
        model = system.getInfo( "model" ),
        platform = system.getInfo( "platform" ),
        platformVersion = system.getInfo( "platformVersion" ),
        targetAppStore = system.getInfo( "targetAppStore" )
	}

	-- Do we have a valid callback function?
	if self._callback and type( self._callback ) == "function" then

		-- Call it and get the extra stuff, then loop through it
		for k, v in pairs( self._callback( event ) or {} ) do

			-- Storing the paramaters in our feedback table
			feedback[ k ] = v

		end

	end

	-- Return it
	return feedback

end

--- Creates a feedback file and saves it to disk.
-- @param error The feedback table.
-- @return The filename and path.
function library:_createFeedbackFile( feedback )

	-- Do we have an error?
	if feedback then

		-- Generate a filename for the local error file
		local filename = time() .. ".feedback"

		-- Create the path
		local path = pathForFile( filename, system.DocumentsDirectory )

		-- Open the file for appending
		local file, errorString = open( path, "w" )

		-- Do we have a file handle?
		if file then

			-- Andcode the error and write it to the file
		   file:write( encode( feedback ) )

		   -- Close the file handle
		   close( file )

		else

			-- Error occurred; output the cause
			print( "File error: " .. errorString )

		end

		-- Nil out the handle
		file = nil

		-- Return the filename and path
		return filename, path

	end

end

--- Submits the feedback to an online API url, if registered.
-- @param error The feedback table.
function library:_submitFeedback( filename, path )

	-- Do we have an api url?
	if self._apiURL and system.getInfo( "platform" ) ~= "nx64" then

		-- Listener for network calls
		local function networkListener( event )

			-- Was there an error? ( Oh the ironing! )
			if event.isError then
				print( "Network error: ", event.response )
			else
				print( event.response)
				-- Was the response a 1? i.e. successfullys submitted
				if event.response == "1" then

					-- Delete the crash file as it was submitted successfully
					remove( path )

				end

			end
		end

		-- Finally upload it
		network.upload(
			self._apiURL,
			"POST",
			networkListener,
			filename,
			system.DocumentsDirectory,
			"application/json"
		)

	end

end

--- Submits any local feedback files that haven't been submitted yet.
function library:_submitFeedbackFiles()

	-- Calculate the correct dir seperator
	local seperator = package.config:sub( 1, 1 )

	function listfiles( dir, list )
		list = list or {}	-- use provided list or create a new one

		for entry in lfs.dir(dir) do
			if entry ~= "." and entry ~= ".." then
				local ne = dir .. seperator .. entry
				if lfs.attributes( ne ).mode == 'directory' then
					listfiles( ne,list )
				else
					insert( list, ne )
				end
			end
		end

		return list

	end

	-- Calculate the path for the DocumentsDirectory
	local dirPath = system.pathForFile( "", system.DocumentsDirectory )

	-- Loop through all files in the directory
	for _, fullPath in ipairs( listfiles( dirPath ) ) do

		-- Is this an error file?
		if find( fullPath, self._extension ) then

			-- Calculate the filename
			local filename = sub( fullPath, -self._filenameLength )

			-- Calculate the file path
			local path = pathForFile( filename, system.DocumentsDirectory )

			-- Submit the error online
			self:_submitFeedback( filename, path )

		end

	end

end

function library:close()
	self._isOpen = false
	self._visual.isVisible = false
	self._textBox.isVisible = false
	native.setKeyboardFocus( nil )

	-- Delete the screenshot file
	remove( pathForFile( self._screenshot.filename, system.DocumentsDirectory ) )

end

function library:isOpen()
	return self._isOpen
end

function library:disable()
	self._disabled = true
end

function library:enable()
	self._disabled = false
end

function library:isDisabled()
	return self._disabled
end

--- Destroys this library.
function library:destroy()

    -- Unregister the handler for unhandled errors
    Runtime:removeEventListener( "key", self )

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Error library
if not Scrappy.Feedback then

	-- Then store the library out
	Scrappy.Feedback = library

end

-- Return the new library
return library
